package assign.domain;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 3 REST API (GETs) and Functional Testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@XmlRootElement(name = "meetings")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meetings {

  private List<String> year = null;

  public List<String> getMeetings() {
    return year;
  }

  public void setMeetings(List<String> meetings) {
    this.year = meetings;
  }
}
