package assign.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 3 REST API (GETs) and Functional Testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class EavesdropService {

  private static final String OPEN_STACK_URL = "http://eavesdrop.openstack.org/meetings";
  private static final String[] SKIP_CASE =
      {"Name", "Last modified", "Size", "Description", "Parent Directory"};

  public EavesdropService() {
    try {
      Jsoup.connect(OPEN_STACK_URL).timeout(10000).execute();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public List<String> getAllProjects() {
    ArrayList<String> projectsList = new ArrayList<String>();
    try {
      Document document = Jsoup.connect(OPEN_STACK_URL).get();
      Elements elements = document.getElementsByAttribute("href");
      for (Element e : elements) {
        String project = e.html();
        if (!Arrays.asList(SKIP_CASE).contains(project)) {
          projectsList.add(project);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return projectsList;
  }

  public List<String> getAllMeetings(String project) {
    ArrayList<String> meetingsList = new ArrayList<String>();
    try {
      Document document = Jsoup.connect(OPEN_STACK_URL).get();
      Elements elements = document.getElementsByAttributeValue("href", project + "/");
      if (elements != null && !elements.isEmpty()) {
        document = Jsoup.connect(elements.attr("abs:href")).get();
        elements = document.getElementsByAttribute("href");
        for (Element e : elements) {
          String year = e.html();
          if (!Arrays.asList(SKIP_CASE).contains(year)) {
            meetingsList.add(year);
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return meetingsList;
  }
}
