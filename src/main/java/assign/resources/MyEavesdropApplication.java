package assign.resources;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 3 REST API (GETs) and Functional Testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@ApplicationPath("/myeavesdrop")
public class MyEavesdropApplication extends Application {

  private Set<Object> singletons = new HashSet<Object>();
  private Set<Class<?>> classes = new HashSet<Class<?>>();

  public MyEavesdropApplication() {}

  @Override
  public Set<Class<?>> getClasses() {
    classes.add(MyEavesdropResource.class);
    return classes;
  }

  @Override
  public Set<Object> getSingletons() {
    return singletons;
  }
}
