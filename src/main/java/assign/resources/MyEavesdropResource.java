package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import assign.domain.Error;
import assign.domain.Meetings;
import assign.domain.Projects;
import assign.services.EavesdropService;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 3 REST API (GETs) and Functional Testing
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@Path("/projects")
public class MyEavesdropResource {

  EavesdropService eavesdropService;

  public MyEavesdropResource() {
    this.eavesdropService = new EavesdropService();
  }

  @GET
  @Path("/")
  @Produces("application/xml")
  public StreamingOutput getAllProjects() throws Exception {

    final Projects projects = new Projects();
    projects.setProjects(this.eavesdropService.getAllProjects());

    return new StreamingOutput() {
      public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        outputProjects(outputStream, projects);
      }
    };
  }

  @GET
  @Path("/{project}/meetings")
  @Produces("application/xml")
  public StreamingOutput getAllMeetings(@PathParam("project") final String project)
      throws Exception {

    // Make project parameter case-sensitive
    if (!project.toLowerCase().equals(project)) {
      return new StreamingOutput() {
        public void write(OutputStream outputStream) throws IOException, WebApplicationException {
          final Error error = new Error();
          error.setError("Project " + project + " does not exist");
          outputError(outputStream, error);
        }
      };
    }

    final Meetings meetings = new Meetings();
    meetings.setMeetings(this.eavesdropService.getAllMeetings(project));

    return new StreamingOutput() {
      public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        if (meetings.getMeetings() == null || meetings.getMeetings().isEmpty()) {
          final Error error = new Error();
          error.setError("Project " + project + " does not exist");
          outputError(outputStream, error);
        } else
          outputMeetings(outputStream, meetings);
      }
    };
  }

  protected void outputProjects(OutputStream os, Projects projects) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Projects.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(projects, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }

  protected void outputMeetings(OutputStream os, Meetings meetings) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(meetings, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }

  protected void outputError(OutputStream os, Error error) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Error.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(error, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }
}
